# talks

the talks I gave at various times

## about

- [web frameworks overview](./web-frameworks-overview/)

---

## license

The project `'talks'` is licensed under the CC0-1.0.

See [LICENSE](./LICENSE) for more details.

The latest source code can be retrieved from one of several mirrors:

1. [github.com/valera-rozuvan/talks](https://github.com/valera-rozuvan/talks)

2. [gitlab.com/valera-rozuvan/talks](https://gitlab.com/valera-rozuvan/talks)

3. [git.rozuvan.net/talks](https://git.rozuvan.net/talks)

Copyright (c) 2015-2022 [Valera Rozuvan](https://valera.rozuvan.net/)
